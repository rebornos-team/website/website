# Changelog
Changelog includes the raw markdown files needed to update the changelog on the RebornOS Website
# files
Files include animations, logos and other branding material used on the website
# Documentation
Documentation file includes documentation on editing the changelog and other website functions.
